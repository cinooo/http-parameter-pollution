'use strict';

// requirements
const express = require('express');
const payment = require('./payment');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

const MININT = 0
const MAXINT = Number.MAX_SAFE_INTEGER

class Amount  {
  constructor(value) {
    if (typeof value !== 'string') {
      throw new Error('amount must be defined and a string')
    }

    if (Number.isNaN(value)) {
      throw new Error('amount must be a valid number')
    }

    const amount = Number(value);

    if (amount < MININT) {
        throw new RangeError('amount to transfer must be greater than 0');
    }

    if (amount > MAXINT) {
        throw new RangeError(`amount to transfer must be less than ${MAXINT}`);
    }

    this.amount = amount
    Object.freeze(this)
  }
}

class Action {
  constructor(action) {
    if (typeof action === 'undefined') {
      throw new Error('action must be defined')
    }

    if (typeof action !== 'string') {
      throw new Error('action must be a string')
    }

    if (action !== 'transfer') {
      throw new Error('You can only transfer an amount')
    }

    this.action = action
    Object.freeze(this)
  }
}

app.get('/', (req, res) => {
    const { action, amount } = req.query || {}
    let act, amt;

    try {
      if (typeof action !== 'string' || typeof amount !== 'string') {
        throw new Error('action & amount must be specified')
      }

      amt = new Amount(amount)
      act = new Action(action)
    } catch (e) {
      res.status(400).send(e.message);
    }

    if (act && act.action === 'transfer') {
        return res.send(payment(act.action, amt.amount))
    }

    res.status(400).send('You can only transfer an amount');
});

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = app;
